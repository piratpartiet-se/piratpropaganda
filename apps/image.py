from typing import Tuple
from PIL import Image, ImageDraw, ImageFont
from pathlib import Path
import os


def generate_image(label: str, image: Image, new_size, logo_size):
    # Checks where the project is located
    project_directory = Path(os.getcwd())

    logo = Image.open(project_directory / "images/logga_och_text2.png").convert("RGBA")

    # Debug values
    print("Katarina bilden ", image.size, image.format, image.mode, project_directory)
    print("PP loggan ", logo.size, logo.format, logo.mode)

    box = calculate_crop(image.size, new_size)
    print("Katarina BOX ", box)
    image = image.crop(box)
    image = image.resize(new_size, Image.LANCZOS)

    box = calculate_crop(logo.size, logo_size)
    logo = logo.crop(box)
    logo = logo.resize(logo_size, Image.LANCZOS)

    print((
        new_size[0] - int(new_size[0] / 20),
        new_size[1] - int(new_size[1] / 20)
    ))
    print(box)
    image.alpha_composite(logo, (
        0, 0
    ))

    font_type = ImageFont.truetype(os.path.join(project_directory, "fonts/Lato-Light.ttf"), 64)

    print("The Images resolution ", image.size)
    print("The Logo resolution ", logo.size)

    center_text(image, font_type, label, new_size)
    image.save("static/new_image.png")
    return "new_image.png"


def calculate_crop(image_size: Tuple[int, int], new_size: Tuple[int, int]) -> Tuple[float, float, float, float]:
    aspect_ratio = new_size[1] / new_size[0]
    crop_height = image_size[0] * aspect_ratio
    return (
        0.0,
        (image_size[1] - crop_height) / 2,
        image_size[0],
        (image_size[1] - crop_height) / 2 + crop_height
    )


# This function calculates where the middle of the images is and places the  text there
def center_text(img, font, text, size: Tuple[int, int], color=(255, 255, 255)):
    draw = ImageDraw.Draw(img)
    text_width, text_height = draw.textsize(text, font)
    position = ((size[0] - text_width) / 2, (size[1] - text_height) / 1.25)
    draw.text(position, text, color, font=font)
    return img
