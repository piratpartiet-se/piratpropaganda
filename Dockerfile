FROM python:3.8-buster
COPY . app/
RUN pip install -r app/requirements.txt
EXPOSE 5000
WORKDIR /app/
CMD ["python", "server.py" ]