from flask import Flask, request, render_template
import apps.image
import PIL

app = Flask(__name__)


@app.route('/')
def hello_word():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def return_image():
    katarina = PIL.Image.open("images/PP_Katarina.jpeg").convert("RGBA")
    image = apps.image.generate_image(request.form['text'], katarina, (1920, 1080), (366, 387))
    return render_template('render.html', picture=image)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)